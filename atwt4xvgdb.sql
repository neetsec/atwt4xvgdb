/** Tabla Transas **/

DROP TABLE IF EXISTS public.transas;
DROP SEQUENCE IF EXISTS public.transas_transa_id; 

CREATE TABLE public.transas(
    address_rec CHAR(34) NOT NULL,
    txid CHAR(64) NOT NULL,
    momento TIMESTAMP DEFAULT NOW(),
    amount DOUBLE PRECISION NOT NULL DEFAULT 0,
    CONSTRAINT transas_pk PRIMARY KEY (address_rec)
);

COMMENT ON TABLE public.transas IS 'Tabla de transacciones que participan en los proyectos.';
COMMENT ON COLUMN public.transas.address_rec IS 'Dirección para que realice el pago del ticket y llave primaria.';
COMMENT ON COLUMN public.transas.txid IS 'Dirección que realiza la transacción.';
COMMENT ON COLUMN public.transas.momento IS 'momento en que ocurre la transacción.';
COMMENT ON COLUMN public.transas.amount IS 'Monto de la transacción.';

/** Tabla Proyectos **/

DROP TABLE IF EXISTS public.proyectos;
DROP SEQUENCE IF EXISTS public.proyectos_proyecto_id; 

CREATE SEQUENCE public.proyectos_proyecto_id;

CREATE TABLE public.proyectos(
    proyecto_id INTEGER NOT NULL DEFAULT nextval('public.proyectos_proyecto_id'),
    account VARCHAR(250) NOT NULL DEFAULT '',
    balance DOUBLE PRECISION NOT NULL,
    fecha TIMESTAMP NOT NULL,
    ticket_price DOUBLE PRECISION NOT NULL DEFAULT 0,
    CONSTRAINT proyectos_pk PRIMARY KEY (proyecto_id)
);

COMMENT ON TABLE public.proyectos IS 'Tabla con los datos de cada proyecto realizado.';
COMMENT ON COLUMN public.proyectos.proyecto_id IS 'Llave primaria.';
COMMENT ON COLUMN public.proyectos.account IS 'Nombre del proyecto.';
COMMENT ON COLUMN public.proyectos.balance IS 'Balance que tiene acumulado el proyecto.';
COMMENT ON COLUMN public.proyectos.fecha IS 'Fecha en que se cierra el proyecto.';
COMMENT ON COLUMN public.proyectos.ticket_price IS 'Precio que tendrá cada ticket del proyecto.';

ALTER SEQUENCE public.proyectos_proyecto_id OWNED BY public.proyectos.proyecto_id;

/** Tabla TxRep **/

DROP TABLE IF EXISTS public.txrep;

CREATE TABLE public.txrep(
    urlhash CHAR(16) NOT NULL,
    wallet CHAR(34) NOT NULL,
    address_rec CHAR(34) NOT NULL,
    momento TIMESTAMP NOT NULL DEFAULT NOW(),
    status INTEGER NOT NULL DEFAULT 1,
    proyecto_id INTEGER,
    CONSTRAINT txrep_pk PRIMARY KEY (urlhash)
);

COMMENT ON TABLE public.txrep IS 'Tabla de reportes hechos por los participantes de transacciones para el proyecto.';
COMMENT ON COLUMN public.txrep.urlhash IS 'Hash usado como URL para ver el estado de la transacción.';
COMMENT ON COLUMN public.txrep.wallet IS 'Dirección a la que se puede hacer devolución.';
COMMENT ON COLUMN public.txrep.address_rec IS 'Dirección en la que se hace el depósito.';
COMMENT ON COLUMN public.txrep.momento IS 'Instante en que se registra un ticket.';
COMMENT ON COLUMN public.txrep.status IS 'Estado del ticket, al darse de alta y esperar pagos es 1, si se confirma el pago 2, al cerrarse el proyecto y quedar inválido 0.';
COMMENT ON COLUMN public.txrep.proyecto_id IS 'Relación del ticket con el proyecto.';
